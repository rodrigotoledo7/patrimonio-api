package br.com.rodrigo.patrimonio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.rodrigo.patrimonio.model.ItemModel;

public interface ItemRepository extends JpaRepository<ItemModel, Long>{

}
