package br.com.rodrigo.patrimonio;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.rodrigo.patrimonio.model.ItemModel;
import br.com.rodrigo.patrimonio.repository.ItemRepository;

@RestController
@CrossOrigin("${origem-permitida}")
public class ItemResource {

	@Autowired
	private ItemRepository itens;

	@GetMapping("/itens")
	public List<ItemModel> listar() {
		return itens.findAll();
	}

	@PostMapping("/itens")
	public ItemModel adicionar(@RequestBody @Valid ItemModel item) {
		return itens.save(item);
	}
}
